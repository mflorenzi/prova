import unittest

import response as response
import self as self
from django.test import  Client
from django.test import  TestCase
from django.contrib.auth.models import User
from django.test import TestCase
from . import models
from datetime import date


# Create your tests here.


class ModelTest(TestCase):

    def setUp(self):
        federico = User(username='fedef', password='1z2xpmon')
        federico.save()
        enrico = User(username='enrico', password='1234')
        enrico.save()
        marco = User(username='m.florenzi', password='1234')
        marco.save()
        andrea = User(username='flor', password='1234')
        andrea.save()

        board1 = models.Board(name='board2', owner=marco)
        board1.save()
        board2 = models.Board(name='marco', owner=marco)
        board2.save()
        board3 = models.Board(name='isw', owner=enrico)
        board3.save()
        board4 = models.Board(name='kikkosboard', owner=enrico)
        board4.save()

        column1 = models.Column(name='col1', board_container=board1)
        column1.save()
        column2 = models.Column(name='col2', board_container=board1)
        column2.save()
        column3 = models.Column(name='col3', board_container=board3)
        column3.save()
        column4 = models.Column(name='col4', board_container=board4)
        column4.save()

    def testModels(self):
        self.assertEqual(len(User.objects.all()), 4)
        self.assertEquals(len(models.Board.objects.all()), 4)
        self.assertEquals(len(models.Column.objects.all()), 4)
        # self.assertEquals(len(models.Card.objects.all()), 3)


class SignUpTest(TestCase):

    def setUp(self):
        self.client = Client()

    def testGetSignUpPage(self):
        response = self.client.get('/signup/')
        self.assertEqual(response.status_code, 200)

    def testSignUpOneUser(self):
        response = self.client.post(

            '/signup/',
            {
                'username': 'luca33',
                'password': 'elucala',

            }, follow=True

        )

        luca = User.objects.get(username='luca33')

        self.assertRedirects(response, '/login/', status_code=302, target_status_code=200, msg_prefix='',
                             fetch_redirect_response=True)

        self.assertEqual(len(User.objects.all()), 1)
        self.assertEqual(luca.username, 'luca33')
        self.assert_(luca.check_password('elucala'))
        self.assertEqual(int(self.client.session['_auth_user_id']), 1)

    def testSignUpTwoUsers(self):
        response = self.client.post(

            '/signup/',
            {
                'username': 'luca33',
                'password': 'elucala',

            }, follow=True

        )

        self.assertRedirects(response, '/login/', status_code=302, target_status_code=200, msg_prefix='',
                             fetch_redirect_response=True)
        self.assertEqual(len(User.objects.all()), 1)
        self.assertEqual(int(self.client.session['_auth_user_id']), 1)

        response = self.client.post(

            '/signup/',
            {
                'username': 'lucia27',
                'password': 'lucidue',

            }, follow=True

        )

        luca = User.objects.get(username='luca33')
        lucia = User.objects.get(username='lucia27')

        self.assertRedirects(response, '/login/', status_code=302, target_status_code=200, msg_prefix='',
                             fetch_redirect_response=True)
        self.assertEqual(len(User.objects.all()), 2)
        self.assertEqual(int(self.client.session['_auth_user_id']), 2)

        self.assertEqual(luca.username, 'luca33')
        self.assert_(luca.check_password('elucala'))

        self.assertEqual(lucia.username, 'lucia27')
        self.assert_(lucia.check_password('lucidue'))

    def testSignUpIncorrectUsername(self):
        response = self.client.post(

            '/signup/',
            {
                'username': 'luca39',
                'password': 'elucala',

            }

        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, len(User.objects.all()))

        response = self.client.post(

             '/signup/',

             {
                'username': 'luca',
                'password': 'elucala',

             }

    )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(User.objects.all()), 0)

        response = self.client.post(

        '/signup/',
        {
            'username': 'luke',
            'password': 'elucala',

        }

    )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(User.objects.all()), 0)

        response = self.client.post(

        '/signup/',
        {
            'username': 'luuu',
            'password': 'elucala',

        }

    )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(User.objects.all()), 0)

        response = self.client.post(

        '/signup/',
        {
            'username': 'ohluca',
            'password': 'elucala',

        }

    )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(User.objects.all()), 0)

    #def testSignUpMissingField(self):
        #response = self.client.post(

         #   '/signup/',
          #  {
           #     'username': '',
            #    'password': 'elucala',

            #}

        #)

          #self.assertEqual(response.status_code, 200)
          #self.assertEqual(len(User.objects.all()), 0)

        #response = self.client.post(

         #   '/signup/',
          #  {
           #     'username': 'luca33',
            #    'password': '',

            #}

        #)

         #self.assertEqual(response.status_code, 200)
         #self.assertEqual(len(User.objects.all()), 0)

    #def testSignUpSameUsername(self):
     #   response = self.client.post(

      #      '/signup/',
       #     {
        #        'username': 'luca33',
         #       'password': 'elucala',

          #  }

        #)

        #self.assertRedirects(response, '/login/', status_code=302, target_status_code=200, msg_prefix='',
         #                    fetch_redirect_response=True)

        #response = self.client.post(

         #   '/signup/',
          #  {
           #     'username': 'luca33',
            #    'password': 'elucala',

            #}

        #)

        #self.assertEqual(response.status_code, 200)
        #self.assertEqual(len(User.objects.all()), 1)

    #def testSignUpIncorrectValues(self):
     #   response = self.client.post(

      #      '/signup/',
       #     {

        #    }

        #)

        #self.assertEqual(response.status_code, 200)
        #self.assertEqual(len(User.objects.all()), 0)

        #response = self.client.post(

         #   '/signup/',
          #  {
           #     'username': 0,
            #    'password': 0,
            #}

        #)

        #self.assertEqual(response.status_code, 200)
        #self.assertEqual(len(User.objects.all()), 0)

        #response = self.client.post(

         #   '/signup/',
          #  {
           #     'username': 0.0,
            #    'password': 0.0,
            #}

        #)

        #self.assertEqual(response.status_code, 200)
        #self.assertEqual(len(User.objects.all()), 0)

        #response = self.client.post(

         #   '/signup/',
          #  {
           #     'username': [],
            #    'password': [],
            #}

        #)

        #self.assertEqual(response.status_code, 200)
        #self.assertEqual(len(User.objects.all()), 0)
