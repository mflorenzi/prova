from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import logout
from django.contrib import messages
from django.db import transaction
from django.db.utils import IntegrityError
from django.contrib.auth import authenticate, login
from scrumboard.models import *
from scrumboard.forms import *
from datetime import date
from . import forms
from . import models
from django.db import IntegrityError
from django.shortcuts import render_to_response

#Tutte le viste, eccetto la gestione del login, logout e registrazione,
#controllano anzitutto che l'utente che accede alla pagina sia autenticato
#(non abbia effettuato un logout). In caso negativo, l'utente viene reindirizzato
#alla pagina di login, con relativo messaggio


#Gestione del login
def loginView(request):

    # Se il form viene compilato e inviato
    if request.method == "POST":  # Accesso alla pagina con metodo POST

        login_form = forms.LoginForm(request.POST)


        if login_form.is_valid():


            if request.user.is_authenticated:
                return redirect(to='dashboard')

            username = login_form.cleaned_data['username']
            password = login_form.cleaned_data['password']
            user = authenticate(username=username, password=password)

            if user is not None:
                # L'autenticazione va a buon fine
                login(request, user)
                return redirect(to='dashboard')

            else:  # Errore: credenziali errate

                messages.error(request, "Username e/o password non sono corretti. Ricontrolla i dati inseriti.")
                return render(request, 'login.html', {'form': forms.LoginForm()})

        else:  # Errore: Form non completamente compilato o dati scorretti

            messages.error(request, "I dati inseriti non sono corretti o mancanti! Ricontrolla i dati inseriti.")
            return render(request, 'login.html', {'form': forms.LoginForm()})

    else:  # Accesso alla pagina con metodo GET
         return render(request, 'login.html', {'form': forms.LoginForm()})
    

#Registrazione di un nuovo utente.
#Questa vista controlla se i dati inseriti nel form per la registrazione di un utente
#sono corretti. In caso positivo, crea un nuovo utente e lo invita a loggarsi con
#le credenziali appena create.
def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():

            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            return redirect('login')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})

#Creazione di una nuova board.
#Questa vista controlla se i dati inseriti nel form
#sono corretti, e in caso positivo salva la board appena creata.
def createNewBoardView(request):
    if request.method == 'POST':
        try:
            form = CreateNewBoardForm(request.POST)
            if form.is_valid():
                name = form.cleaned_data.get('name')
                current_user = request.user
                board = Board(name=name, author=current_user)
                board.save()
                board.members.add(current_user)
                messages.success(request, 'Board saved succesfully')
                #return redirect('dashboard')
                return render(request, "boardDetail.html",
                              {
                                  'board': board,
                                  'boardId': board.id
                              })
        except IntegrityError:
            messages.success(request, 'This Board already exists, try with another name')
            return render(request, 'createNewBoard.html', {'form': forms.CreateNewBoardForm()})

    return render(request, 'createNewBoard.html', {'form': forms.CreateNewBoardForm()})

#Questa vista mostra la pagina iniziale all'utente, come un insieme di board.
def dashboard(request):

    try:  # Check esistenza variabile di sessione dello user_id

        user = User.objects.get(pk=request.session['_auth_user_id'])

        if user.is_authenticated:
            board = Board.objects.all()
            return render(request, 'dashboard.html', {'board': board})
        else:
            messages.error(request, "Login, please")
            return redirect(to='login')

    except KeyError:
        messages.error(request, "Login, please")
        return redirect(to='login')

#Questa vista gestisce il logout. Si occupa di fare la Free dei dati relativi alla sessione
#e di redirezionare l'utente alla pagina del login
def logoutView(request):
    logout(request)
    request.session.flush()
    return redirect(to='login')


#Questa vista si occupa di mostrare il dettaglio di una board: le colonne che possiede
def boardDetail(request, boardId):

    try:  # Check esistenza variabile di sessione dello user_id

        user = User.objects.get(pk=request.session['_auth_user_id'])

        if user.is_authenticated:
            try:
                board = models.Board.objects.get(pk=boardId)
            except Board.DoesNotExist:
                board = None

            try:
                columns = models.Column.objects.filter(board=boardId)
            except Column.DoesNotExist:
                columns = None

            try:
                cards = models.Card.objects.filter(board_container=boardId)
            except Column.DoesNotExist:
                cards = None


            return render(request, "boardDetail.html",
                          {
                              'board': board,
                              'boardId': boardId,
                              'columns': columns,
                              'cards': cards
                          })

        else:
            messages.error(request, "Login, please")
            return redirect(to='login')

    except KeyError:
        messages.error(request, "Login, please")
        return redirect(to='login')


#Questa vista si occupa della cancellazione di una board
def boardDelete(request,boardId):
    try:  # Check esistenza variabile di sessione dello user_id

        user = User.objects.get(pk=request.session['_auth_user_id'])

        if user.is_authenticated:

            board = models.Board.objects.get(pk=boardId)
            current_user = request.user

            #Solo il creatore della board la può cancellare
            if board.author == current_user:
                if request.method == 'POST':
                    board.delete()
                    return redirect(to='dashboard')

            return render(request, "boardDelete.html",{
                              'board': board,
                              'boardId': boardId
                          })
        else:
            messages.error(request, "Login, please")
            return redirect(to='login')
    except KeyError:
        messages.error(request, "Login, please")
        return redirect(to='login')

#Questa vista gestisce la creazione di una nuova colonna nella board il cui id è specificato nella URL
def createNewColumnView(request,boardId):

    try:  # Check esistenza variabile di sessione dello user_id

        user = User.objects.get(pk=request.session['_auth_user_id'])

        if user.is_authenticated:

            board = models.Board.objects.get(pk=boardId)
            if user in board.members.all():
                if request.method == 'POST':
                    try:
                        form = CreateNewColumnForm(request.POST)
                        if form.is_valid():
                            name = form.cleaned_data.get('name')
                            column = Column(name=name, board=board)
                            column.save()

                            messages.success(request, 'Column saved succesfully')
                            return render(request, "createNewColumn.html")

                    except IntegrityError:
                        messages.success(request, 'This Column already exists, try with another name')
                        return render(request, 'createColumn', {'form': forms.CreateNewColumnForm()})

                return render(request, 'createNewColumn.html', {'form': forms.CreateNewColumnForm()})
            else:
                messages.error(request,"non autorizzato")
                return render(request, "createNewColumn.html")

        else:
            messages.error(request, "Login, please")
            return redirect(to='login')
    except KeyError:
        messages.error(request, "Login, please")
        return redirect(to='login')

#Questa vista si occupa della cancellazione di una colonna.
def columnDelete(request, boardId, columnId):
    try:  # Check esistenza variabile di sessione dello user_id

        user = User.objects.get(pk=request.session['_auth_user_id'])

        if user.is_authenticated:
            board = models.Board.objects.get(pk=boardId)
            column = models.Column.objects.get(pk=columnId)
            current_user = request.user

            #Solo l'autore della board può cancellare la colonna
            if board.author == current_user:
                if request.method == 'POST':
                    column.delete()
                    return redirect(to='dashboard')

            return render(request, "columnDelete.html", {
                'boardId' :boardId,
                'columnId':columnId,
                'column':column
            })
        else:
            messages.error(request, "Login, please")
            return redirect(to=login)
    except KeyError:
        messages.error(request, "Login, please")
        return redirect(to=login)


#Vista che gestisce gli utenti membri di una board
def addUser(request,boardId):
    board = models.Board.objects.get(pk=boardId)
    try:  # Check esistenza variabile di sessione dello user_id

        user = User.objects.get(pk=request.session['_auth_user_id'])

        if user.is_authenticated:
            if user in board.members.all():
                board = models.Board.objects.get(pk=boardId)

                members = [(nm.id, nm.username) for nm in User.objects.exclude(pk__in=board.members.all())]


                form = forms.AddUserForm(request.POST)
                if request.method == 'POST':
                    if form.is_valid():
                        user = form.cleaned_data.get('users')
                        with transaction.atomic():
                            added_user=User.objects.get(pk=user)
                            board.members.add(added_user)
                            members.remove((added_user.id, added_user.username))
                    else:
                        messages.error("no")



                return render(request, 'addUser.html',
                              {'form': forms.AddUserForm().set_user_list(members),
                               'boardId': boardId,
                               'board': board})
            else:
                messages.error(request,"non autorizzato")
                return render(request, 'addUser.html')
        else:
            messages.error(request, "Log in please")
            return redirect (to='login')
    except KeyError:
        messages.error(request, "Log in please")
        return redirect(to='login')


#Vista che si occupa di modificare il nome di una colonna e di visualizzare la lista delle card appartenenti a tale colonna
def modifyColumnView(request, boardId, columnId):
    board = models.Board.objects.get(pk=boardId)
    try:  # Check esistenza variabile di sessione dello user_id

        user = User.objects.get(pk=request.session['_auth_user_id'])

        if user.is_authenticated:
            if user in board.members.all():

                column = models.Column.objects.get(pk=columnId)
                cards = models.Card.objects.filter(column_container=columnId)

                if request.method == 'POST':  # Accesso alla pagina in metodo POST
                    try:

                        modify_column_form = forms.ModifyColumnForm(request.POST)

                        # Se il form e' valido
                        if modify_column_form.is_valid():
                        # modifica della colonna
                            column.name = modify_column_form.cleaned_data.get('name')
                            column.save()
                            messages.success(request, "Column's name modified succesfully")


                            return render(request, "modifyColumn.html",
                                  {
                                      'boardId': boardId,
                                      'columnId': column.id,
                                      'cards': cards
                                  })
                    except IntegrityError:
                        messages.error(request, 'This Column already exists, try with another name')
                        return render(request, 'modifyColumn.html', {'form': forms.ModifyColumnForm(),
                                      'cards': cards})

                return render(request, 'modifyColumn.html', {'form': forms.ModifyColumnForm(),
                                      'cards': cards})
            else:
                messages.error(request, "non autorizzato")
                return render(request, 'modifyColumn.html')
        else:
            messages.error(request, "Login, please")
            return redirect(to='login')
    except KeyError:
        messages.error(request, "Login, please")
        return redirect(to='login')


#Vista che mostra il burndown della board
def burndownView(request, boardId):
    try:  # Check esistenza variabile di sessione dello user_id

        user = User.objects.get(pk=request.session['_auth_user_id'])

        if user.is_authenticated:

            #calcolo del numero totale delle card della board
            cards = models.Card.objects.filter(board_container=boardId)
            totalCards= len(cards)

            #costruzione della lista contenente per ogni colonna, il numero di cards
            columns = models.Column.objects.filter(board=boardId)
            cardsInColumn = []
            for col in columns:
                cardsInColumn.append({'name': col.name, 'numberOfCardsForCol': len(cards.filter(column_container=col.id))})

            #calcolo dei punti storia totali per la board
            storyPoints = 0
            for card in cards:
                storyPoints = storyPoints + card.story_points

            #calcolo del numero di card scadute
            numberOfExpiredCards = len(cards.filter(date_deadline__lt=date.today()))

            return render(request, 'burndown.html', {'totalCards':totalCards,
                                                     'boardId': boardId,
                                                     'storyPoints': storyPoints,
                                                     'cardsInColumn':cardsInColumn,
                                                     'numberOfExpiredCards': numberOfExpiredCards,
                                                     'board': models.Board.objects.get(pk=boardId)})
        else:
            messages.error(request, "Login, please")
            return redirect(to=login)
    except KeyError:
        messages.error(request, "Login, please")
        return redirect(to='login')


#Vista che gestisce la creazione di una nuova card
def createNewCard(request, boardId):
    user = User.objects.get(pk=request.session['_auth_user_id'])
    board = Board.objects.get(pk=boardId)
    # column_list = [(nm.id, nm.name) for nm in Column.objects.exclude(pk__in())]

    column_list = [(nm.id, nm.name) for nm in models.Column.objects.filter(board=board)]

    try:  # Check esistenza variabile di sessione dello user_id
        if user.is_authenticated:
            if user in board.members.all():
                if request.method == 'POST':
                    try:
                        form = forms.CreateNewCardForm(request.POST)
                        if form.is_valid():

                            name = form.cleaned_data.get('name')
                            description = form.cleaned_data.get('description')
                            date_creation = date.today()
                            date_deadline = form.cleaned_data.get('date_deadline')
                            story_points = form.cleaned_data.get('story_points')
                            column = models.Column.objects.get(pk=form.cleaned_data.get('column'))

                            card = Card(title=name, description=description, date_creation=date_creation, date_deadline=date_deadline, story_points=story_points, board_container=board, column_container=column)
                            card.save()
                            messages.success(request, 'Card saved succesfully')
                            return render(request, "createNewCard.html", {'boardId': boardId})

                    except IntegrityError:
                        messages.success(request, 'This Card already exists, try with another name')
                        return render(request, 'createNewCard.html', {'form': forms.CreateNewCardForm().set_column_list(column_list),'boardId': boardId})

                return render(request, 'createNewCard.html', {'form': forms.CreateNewCardForm().set_column_list(column_list), 'boardId': boardId})

            else:
                messages.error(request,"non autorizzato")
                return render(request, 'createNewCard.html')
        else:
            messages.error(request, "Login, please")
            return redirect(to='login')
    except KeyError:
        messages.error(request, "Login, please")
        return redirect(to='login')


def cardDelete(request, boardId, cardId):
    try:  # Check esistenza variabile di sessione dello user_id

        user = User.objects.get(pk=request.session['_auth_user_id'])

        if user.is_authenticated:
            board = models.Board.objects.get(pk=boardId)
            card = models.Card.objects.get(pk=cardId)
            current_user = request.user

            #Solo l'autore della board può cancellare la card
            if board.author == current_user:
                if request.method == 'POST':
                    card.delete()
                    return redirect(to='dashboard')

            return render(request, "cardDelete.html", {
                'board' :board,
                'card' : card,
            })
        else:
            messages.error(request, "Login, please")
            return redirect(to=login)
    except KeyError:
        messages.error(request, "Login, please")
        return redirect(to=login)



def modifyCardView(request, boardId, cardId):
    board = Board.objects.get(pk=boardId)
    # column_list = [(nm.id, nm.name) for nm in Column.objects.exclude(pk__in())]

    column_list = [(nm.id, nm.name) for nm in models.Column.objects.filter(board=board)]
    try:  # Check esistenza variabile di sessione dello user_id

        user = User.objects.get(pk=request.session['_auth_user_id'])

        if user.is_authenticated:
            board = models.Board.objects.get(pk=boardId)
            card = models.Card.objects.get(pk=cardId)
            current_user = request.user

            #Solo l'autore della board può modificare la card
            if board.author == current_user:
                if request.method == 'POST':
                    modify_card_form = forms.ModifyCardForm(request.POST)

                    # Se il form e' valido
                    if modify_card_form.is_valid():
                        # modifica dei dati
                        card.title = modify_card_form.cleaned_data.get('name')
                        card.description = modify_card_form.cleaned_data.get('description')
                        card.date_deadline = modify_card_form.cleaned_data.get('date_deadline')
                        card.story_points = modify_card_form.cleaned_data.get('story_points')
                        card.column_container = models.Column.objects.get(pk=modify_card_form.cleaned_data.get('column'))
                        card.save()

                        messages.success(request, "Card has been modified succesfully")

                        return render(request, "cardModify.html",
                                      {'form': forms.ModifyCardForm().set_column_list(column_list),
                                       'board': board,
                                       'card': card})

                return render(request, "cardModify.html",
                              {'form': forms.ModifyCardForm().set_column_list(column_list),
                               'board': board,
                               'card': card})

            return render(request, "cardModify.html",
                {'form': forms.ModifyCardForm().set_column_list(column_list),
                'board': board,
                'card' : card})
        else:
            messages.error(request, "Login, please")
            return redirect(to=login)
    except KeyError:
        messages.error(request, "Login, please")
        return redirect(to=login)