from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from django.conf import settings
from django.urls import reverse

class Board(models.Model):
    name = models.CharField(max_length=15,unique = True, null=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE, null=True,related_name='author')
    date = models.DateField(default=datetime.now)
    members = models.ManyToManyField(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name.__str__()

    def get_absolute_url(self):
        return reverse(viewname='boardDetail', args=[str(self.id)])


class Column(models.Model):
    name = models.CharField(max_length=15, unique=True)
    board = models.ForeignKey(Board, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name.__str__()

    def get_absolute_url(self):
        return reverse(viewname='ColumnDetail', args=[str(self.id)])


class Card(models.Model):

    title = models.CharField(max_length=25)
    description = models.CharField(max_length=50)
    members = models.ManyToManyField(settings.AUTH_USER_MODEL)
    date_creation = models.DateField()
    date_deadline = models.DateField()
    story_points = models.SmallIntegerField()
    board_container = models.ForeignKey('Board', on_delete=models.CASCADE)
    column_container = models.ForeignKey('Column', on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse(viewname='CardDetail', args=[str(self.id)])