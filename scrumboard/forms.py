from django import forms
from django.core.validators import MinValueValidator
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from scrumboard.models import *



class LoginForm(forms.Form):

    username = forms.CharField(label='Username ', max_length=30)
    password = forms.CharField(label='Password ', max_length=30, widget=forms.PasswordInput)



class CreateNewBoardForm(forms.Form):

    name = forms.CharField(label='Board name:',max_length=30)


class CreateNewColumnForm(forms.Form):
    name = forms.CharField(label='Column name:', max_length=30)

class AddUserForm(forms.Form):
    users = forms.IntegerField(label= 'Users')

    def set_user_list(self,user_list):
        self.fields['users'].widget = forms.Select(choices=user_list)
        return self

class ModifyColumnForm(forms.Form):
    name = forms.CharField(label='New name:', max_length=30)


class CreateNewCardForm(forms.Form):
    name = forms.CharField(label='Title', max_length=25)
    description = forms.CharField(label='Description', max_length=50)
    date_deadline = forms.DateField(label='Expire date', widget=forms.widgets.SelectDateWidget(
        empty_label=('Year', 'Month', 'Day')))
    story_points = forms.IntegerField(label='Story Points', validators=[MinValueValidator(0)])

    column = forms.IntegerField(label='Column')

    def set_column_list(self, column_list):
        self.fields['column'].widget = forms.Select(choices=column_list)
        return self


class ModifyCardForm(forms.Form):
    name = forms.CharField(label='Title', max_length=25)
    description = forms.CharField(label='Description', max_length=50)
    date_deadline = forms.DateField(label='Expire date', widget=forms.widgets.SelectDateWidget(
        empty_label=('Year', 'Month', 'Day')))
    story_points = forms.IntegerField(label='Story Points', validators=[MinValueValidator(0)])

    column = forms.IntegerField(label='Column')


    def set_column_list(self, column_list):
        self.fields['column'].widget = forms.Select(choices=column_list)
        return self