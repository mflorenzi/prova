"""p0 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from scrumboard import views
#push 2florenzi
#push 3florenzi
urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.loginView,name = 'login'),
    path(r'logout/', views.logoutView, name='logout'),
    path(r'signup/', views.signup,name = 'signup'),
    path(r'dashboard/', views.dashboard,name='dashboard'),
    path(r'createNewBoard/', views.createNewBoardView,name='createNewBoard'),
    path(r'dashboard/board/<boardId>/', views.boardDetail, name='board'),
    path(r'dashboard/board/<boardId>/', views.boardDetail, name='boardDetail'),
    path(r'dashboard/boardDelete/<boardId>/', views.boardDelete, name='boardDelete'),
    path(r'dashboard/board/<boardId>/createColumn/', views.createNewColumnView, name='createColumn'),
    path(r'dashboard/board/<boardId>/columnDelete/<columnId>/', views.columnDelete, name='columnDelete'),
    path(r'dashboard/board/<boardId>/AddUser', views.addUser, name='addUser'),
    path(r'dashboard/board/<boardId>/modifyColumn/<columnId>', views.modifyColumnView, name='modifyColumn'),
    path(r'dashboard/board/<boardId>/burndown', views.burndownView, name='burndown'),
    path(r'dashboard/board/<boardId>/createCard/', views.createNewCard, name='createCard'),
    path(r'dashboard/board/<boardId>/modifyColumn/cardDelete/<cardId>/', views.cardDelete, name='cardDelete'),
    path(r'dashboard/board/<boardId>/modifyColumn/cardModify/<cardId>/', views.modifyCardView, name='modifyCard')
]
